const PckJs = (function(rootSelector) {
    const cloneDom = (elem) => {
        let e = new Elem(elem.tagName, [], elem.content, [], []);

        e.path(elem.path());

        elem.attr.forEach((val, key) => {
            e.attr.set(key, val);
        })

        elem.binds.forEach((val, key) => {
            e.binds.set(key, val);
        });

        for (let i = 0, ii = elem.children.length; i < ii; i++) {
            e.append(elem.children[i]);
        }

        return e;
    };

    const findHtmlElem = (path) => {
        let htmlElem = document.querySelector(rootSelector);
        path.shift();

        while (path.length) {
            htmlElem = htmlElem.children[path.shift()];
        }

        return htmlElem;
    };

    const dataFallback = (entry) => {
        let match = typeof entry === 'string' ? entry.match(/^\@(.+)$/) : null;
        return (match && data.has(match[1])) ? data.get(match[1]) : entry;
    };

    const pathize = (dom, path) => {
        path = path || 'x';
        dom.path(path);

        for (let i = 0, ii = dom.children.length; i < ii; i++)
            pathize(dom.children[i], path + '.' + i);
    };

    const diffOld = (d) => {
        let t = newDom.find(d.path().split('.'));

        if (typeof diffs[d.path()] === 'undefined')
            diffs[d.path()] = [];

        if (t.tagName == null) {
            diffs[d.path()].push({action: 'remove'});
        }
        else {
            if (t.content != d.content) {
                diffs[d.path()].push({action: 'setContent', content: d.content});
            }

            t.attr.forEach((val, key) => {
                if (!d.attr.has(key)) {
                    diffs[d.path()].push({action: 'removeAttr', content: key});
                }
                else if ((val instanceof Array && !compareArrayContents(val, d.attr.get(key))) || d.attr.get(key) != val) {
                    diffs[d.path()].push({action: 'updateAttr', content: key, value: d.attr.get(key)});
                }
            });

            t.binds.forEach((val, key) => {
                if (!d.binds.has(key)) {
                    diffs[d.path()].push({action: 'removeBind', content: key});
                }
            });
        }

        for (let i = 0, ii = d.children.length; i < ii; i++)
            diffOld(d.children[i]);
    };

    const diffNew = (d) => {
        let t = oldDom.find(d.path().split('.'));

        if (typeof diffs[d.path()] === 'undefined')
            diffs[d.path()] = [];

        if (!t) {
            diffs[d.path()].push({action: 'add', content: d});
        }
        else {
            d.attr.forEach((val, key) => {
                if (!t.attr.has(key)) {
                    diffs[d.path()].push({action: 'addAttr', content: key, value: val});
                }
            });

            d.binds.forEach((val, key) => {
                if (!t.binds.has(key)) {
                    diffs[d.path()].push({action: 'addBind', content: key, value: val});
                }
            });

            for (let i = 0, ii = d.children.length; i < ii; i++)
                diffNew(d.children[i]);
        }
    };

    const compareArrayContents = (arr1, arr2) => {
        if (arr1.length != arr2.length)
            return false;

        for (let i = 0, ii = arr1.length; i < ii; i++) {
            if (arr2.indexOf(arr1[i]) == -1) {
                return false;
            }
        }
        return true;
    };

    const refresh = () => {
        pathize(newDom);

        data.setRefresh(false);

        diffs = {};

        diffOld(oldDom);
        diffNew(newDom);

        let htmlElem, operationPath, operationIdx, elem;

        for (let i in diffs) {
            if (!diffs.hasOwnProperty(i) || !diffs[i].length)
                continue;

            htmlElem = findHtmlElem(i.split('.'));

            diffs[i].forEach((val, key) => {
                switch (val.action) {
                    case 'add':
                        operationPath = i.split('.');
                        operationPath.pop();

                        htmlElem = findHtmlElem(operationPath);
                        htmlElem.appendChild(render(val.content));
                        break;

                    case 'remove':
                        operationPath = i.split('.');
                        operationIdx = operationPath.pop();

                        htmlElem = findHtmlElem(operationPath);
                        htmlElem.removeChild(htmlElem.childNodes[operationIdx]);

                        operationPath = i.split('.');
                        operationIdx = operationPath.pop();

                        elem = newDom.find(operationPath);
                        elem.children.splice(operationIdx, 1);
                        break;

                    case 'setContent':
                        htmlElem.innerText = dataFallback(val.content);
                        break;

                    case 'addAttr':
                    case 'updateAttr':
                        htmlElem.setAttribute(val.content, val.value);
                        break;

                    case 'removeAttr':
                        htmlElem.removeAttribute(val.content);
                        break;

                    case 'addBind':
                        elem = newDom.find(i.split('.'));
                        htmlElem.addEventListener(val.listener, (t) => {val.callback(t, elem); refresh();}, false);
                        break;

                    case 'removeBind':
                        htmlElem.removeEventListener(val.listener);
                        break;
                }
            });
        }

        data.setRefresh(true);

        oldDom = cloneDom(newDom);

        return;
    };

    const render = (elem) => {
        let e = document.createElement(elem.tagName);

        let attrValue;
        elem.children.forEach((c) => {
            e.appendChild(render(c));
        });

        if (['input', 'textarea'].indexOf(elem.tagName) > -1 && elem.hasAttr('name'))
            data.set(elem.getAttr('name'), elem.hasAttr('value') ? elem.getAttr('value') : null);

        elem.attr.forEach((val, key) => {
            attrValue = (val instanceof Array) ? val.join(' ') : val;

            if (typeof e[key] !== 'undefined')
                e[key] = dataFallback(attrValue);
            e.setAttribute(key, dataFallback(attrValue));
        });

        if (elem.content)
            e.textContent = elem.content;

        elem.binds.forEach((val, key) => {
            e.addEventListener(val.listener, (t) => {val.callback(t, elem); refresh();}, false);
        });

        return e;
    };

    const run = () => {
        pathize(newDom);
        document.querySelector(rootSelector).replaceWith(render(newDom));

        oldDom = cloneDom(newDom);

        refresh();
    };

    // constructors

    const Data = (function() {
        let d = {};
        let r = false;
        let datalist = new Map();

        d.setRefresh = (value) => {
            r = value;
        };

        d.get = (variable) => {
            return datalist.get(variable);
        };

        d.set = (variable, value) => {
            datalist.set(variable, value);
            if (r)
                refresh();
        };

        d.del = (variable) => {
            datalist.delete(variable);
            if (r)
                refresh();
        };

        d.has = (variable) => {
            return datalist.has(variable);
        };

        d.list = () => {
            return datalist.entries();
        }

        return d;
    });

    const Elem = (function(tagName, attributes, content, binds, children) {
        children = children || [];

        let myPath = null;

        let e = {
            'tagName': tagName || 'div',
            'attr': new Map(),
            'content': dataFallback(content || null),
            'binds': new Map(),
            'children': []
        };

        for (let i in attributes) {
            if (!attributes.hasOwnProperty(i))
                continue;

            e.attr.set(i, attributes[i]);
        }

        for (let i in binds) {
            if (!binds.hasOwnProperty(i))
                continue;

            e.binds.set(i, binds[i]);
        }

        e.addClass = (className) => {
            if (!e.hasAttr('class')) {
                e.setAttr('class', [className]);
            }
            else if (!e.hasClass(className)) {
                let classes = e.getAttr('class');
                classes.push(className);
                e.setAttr('class', classes);
            }
        }

        e.removeClass = (className) => {
            if (!e.hasClass(className))
                return;

            let classes = e.attr.get('class');
            let index = classes.indexOf(className);
            classes.splice(index, 1);
            e.set('class', classes);
        }

        e.hasClass = (className) => {
            if (!e.attr.has('class'))
                return false;

            let classes = e.attr.get('class');
            return classes.indexOf(className) !== -1;
        }

        e.getAttr = (attr) => {
            return e.attr.get(attr);
        };

        e.setAttr = (attr, value) => {
            e.attr.set(attr, dataFallback(value));

            if (attr == 'value')
                refresh();
        };

        e.removeAttr = (attr) => {
            e.attr.delete(attr);

            if (attr == 'value')
                refresh();
        };

        e.hasAttr = (attr) => {
            return e.attr.has(attr);
        };

        e.path = (path) => {
            if (typeof path === 'string')
                myPath = path;

            for (let i = 0, ii = e.children.length; i < ii; i++) {
                e.children[i].path(myPath + '.' + i);
            }

            return myPath;
        };

        e.bind = (alias, listener, callback) => {
            e.binds.set(alias, {
                'callback': callback,
                'listener': listener || null
            });

            refresh();

            return e;
        };

        e.unbind = (alias) => {
            e.binds.delete(alias);

            refresh();

            return e;
        };

        e.find = (path) => {
            let idx = path.shift();

            if (idx == 'x')
                idx = path.shift();

            if (typeof idx == 'undefined') {
                return e;
            }

            return (typeof e.children[idx] !== 'undefined') ? e.children[idx].find(path) : null;
        };

        e.remove = (path) => {
            if (typeof path === 'undefined') {
                e.tagName = null;
                return;
            }

            let idx = path.shift();

            if (idx == 'x')
                idx = path.shift();


            if (typeof idx === 'undefined') {
                e.tagName = null;
                return;
            }

            if (path.length)
                e.children[idx].remove(path);
            else {
                e.children[idx].tagName = null;
            }
        };

        e.append = (elem, path) => {
            elem.path(e.path() + '.' + e.children.length);

            if (typeof path === 'undefined') {
                e.children.push(cloneDom(elem));

                return;
            }

            let idx = path.shift();

            if (path.length) {
                e.children[idx].append(elem, path);
            }
            else {
                e.children[idx] = cloneDom(elem);
            }
        };

        e.flatten = () => {
            let f = {};
            if (e.hasAttr('id'))
                f.elem = '#' + e.getAttr('id');
            else if (e.hasAttr('class'))
                f.elem = '.' + e.getAttr('class').join('.');
            else
                f.elem = e.tagName;

            f.path = e.path();

            if (e.attr.has('name') && e.attr.has('value')) {
                f.name = e.attr.get('name');
                f.value = e.attr.get('value');
            }

            if (e.children.length) {
                f.kids = [];

                for (let i = 0, ii = e.children.length; i < ii; i++)
                    f.kids.push(e.children[i].flatten());
            }

            return f;
        };

        e.parent = () => {
            let path = e.path().split('.');
            path.pop();

            return (path.length) ? newDom.find(path) : null;
        };

        const autobind = () => {
            let operation;

            switch (e.tagName) {
                case 'button':
                case 'a':
                    operation = 'click';
                    break;

                case 'textarea':
                    operation = 'input';
                    break;

                case 'select':
                    operation = 'change';
                    break;

                case 'input':
                    switch (e.getAttr('type')) {
                        case 'checkbox':
                        case 'radio':
                        case 'file':

                            operation = 'change';
                            break;

                        case 'button':
                        case 'submit':
                            operation = 'click';
                            break;

                        // case 'range':
                        default:
                            operation = 'input';
                            break;
                    }
                    break;
            }

            if (!e.binds.has(operation)) {
                e.binds.set(operation, {
                    'listener': operation,
                    'callback': (t) => {
                        let val = data.get(t.target.name);

                        let newVal = (typeof val === 'string' && val.search(/^@/) == 0) ? val : t.target.value;
                        data.set(t.target.name, newVal);

                        t.target.value = dataFallback(newVal);
                        t.target.setAttribute('value', dataFallback(newVal));
                        refresh();
                    }
                });
            }
        };


        for (let i = 0, ii = children.length; i < ii; i++) {
            e.append(children[i]);
        }

        if (e.hasAttr('name')) {
            autobind();
        }

        if (e.hasAttr('class')) {
            let classes = e.getAttr('class');
            classes = classes instanceof Array ? classes : [classes];
            e.setAttr('class', classes);
        };

        return e;
    });

    let matches = rootSelector.match(/^(.)(.+)$/);
    let attrs = (matches[1] == '#') ? {id: [matches[2]]} : {class: [matches[2]]};
    let diffs = [];
    let newDom = new Elem('div', attrs);

    pathize(newDom);
    let oldDom;

    let data = new Data();

    return {
        version: '0.0.3',
        dom: newDom,
        old: oldDom,
        elem: Elem,
        run: run,
        refresh: refresh,
        data: data,
        cloneDom: cloneDom
    }
});